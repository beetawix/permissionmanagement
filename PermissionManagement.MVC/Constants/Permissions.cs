﻿using System.Collections.Generic;

namespace PermissionManagement.MVC.Constants
{
    public static class Permissions
    {
        public static List<string> GeneratePermissionsForModule(string module)
        {
            return new List<string>()
        {
            $"Permissions.{module}.Create",
            $"Permissions.{module}.View",
            $"Permissions.{module}.Edit",
            $"Permissions.{module}.Delete",
        };
        }

        public static class Products
        {
            public const string View = "Permissions.Products.View";
            public const string Create = "Permissions.Products.Create";
            public const string Edit = "Permissions.Products.Edit";
            public const string Delete = "Permissions.Products.Delete";
        }

        public static class Subjects
        {
            public const string View = "Permissions.Subjects.View";
            public const string Create = "Permissions.Subjects.Create";
            public const string Edit = "Permissions.Subjects.Edit";
            public const string Delete = "Permissions.Subjects.Delete";
        }
    }
}